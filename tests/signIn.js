
const { xpaths } = require('../data/xpaths/sign_in_xpaths');
beforeEach(function(){
	browser.url('/login');
})
describe('Test the sign in functionality',function(){

	it('Can user signin using valid credentials',function(done){

		//browser.windowHandleMaximize();
		const signin_email = xpaths.sign_in_email;
		const signin_password = xpaths.sign_in_password;
		const signin_sumbit_btn = xpaths.sumbit_btn;
		const select_account_to_continue_text = xpaths.select_account_to_continue;
		$(signin_email).setValue('kailash.kumar@yapsody.com');
		$(signin_password).setValue('Ksuthar@1805');
		$(signin_sumbit_btn).click();
		browser.pause(5000);
		const  compare = $(select_account_to_continue_text).getText();
		expect(compare).to.equal('Select Account To Continue');
	});

	it('Show validation error message for mandatory email field',function(done){

		//browser.windowHandleMaximize();
		const signin_email = xpaths.sign_in_email;
		const signin_password = xpaths.sign_in_password;
		const signin_sumbit_btn = xpaths.sumbit_btn;
		$(signin_email).setValue('');
		$(signin_password).setValue('abc');
		$(signin_sumbit_btn).click();
		const email_validation_message = xpaths.email_field_mandatoty_validation_text;
		const  compare = $(email_validation_message).isExisting();
		expect(compare).to.be.true;
	});

	it('Show validation error message for mandatory password field',function(done){

		//browser.windowHandleMaximize();
		const signin_email = xpaths.sign_in_email;
		const signin_password = xpaths.sign_in_password;
		const signin_sumbit_btn = xpaths.sumbit_btn;
		const select_account_to_continue_text = xpaths.select_account_to_continue;
		$(signin_email).setValue('kailash.kumar@yapsody.com');
		$(signin_password).click();
		$(signin_email).click();
		browser.pause(1000);
		const password_validation_message = xpaths.password_field_mandatoty_validation_text;
		const  compare = $(password_validation_message).isExisting();
		expect(compare).to.be.true;
	});

	it('Invalid email address and password',function(done){

		//browser.windowHandleMaximize();
		const signin_email = xpaths.sign_in_email;
		const signin_password = xpaths.sign_in_password;
		const signin_sumbit_btn = xpaths.sumbit_btn;
		$(signin_email).setValue('test@kmail.com');
		$(signin_password).setValue('123123123');
		$(signin_sumbit_btn).click();
		browser.pause(2000);
		const invalid_Login = xpaths.login_failed;
		const  compare = $(invalid_Login).getText();
		expect(compare).to.equal('Please double check the username and the password');
	});

	it('Valid email ID and invalid password',function(done){

		const signin_email = xpaths.sign_in_email;
		const signin_password = xpaths.sign_in_password;
		const signin_sumbit_btn = xpaths.sumbit_btn;
		$(signin_email).setValue('kailash.kumar@yapsody.com');
		$(signin_password).setValue('123123123');
		$(signin_sumbit_btn).click();
		browser.pause(2000);
		const invalid_Login = xpaths.login_failed;
		const  compare = $(invalid_Login).getText();
		expect(compare).to.equal('Please double check the username and the password');

	});

});	