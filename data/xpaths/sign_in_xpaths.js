module.exports = {
    xpaths: {
		sign_in_email: "//input[@id='email']",
        sign_in_password : "//input[@id='password']",
        sumbit_btn : "//button[@type='submit']",
        select_account_to_continue : "//span[@class='title undefined']",
        email_field_mandatoty_validation_text : "//div[@class='help text-brand-red font12px font-italic error-message minheight20px' and text()='Email Field is mandatory']",
        password_field_mandatoty_validation_text : "//div[@class='help text-brand-red font12px font-italic error-message minheight20px' and text()='Password Field is mandatory']",
        login_failed : "//p[@class='help-text' and text()='Please double check the username and the password']"
},
};